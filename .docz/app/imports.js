export const imports = {
  'js/styleguide/colors.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "js-styleguide-colors" */ 'js/styleguide/colors.mdx'),
  'js/styleguide/header.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "js-styleguide-header" */ 'js/styleguide/header.mdx'),
  'js/styleguide/icons.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "js-styleguide-icons" */ 'js/styleguide/icons.mdx'),
  'js/styleguide/links.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "js-styleguide-links" */ 'js/styleguide/links.mdx'),
  'js/styleguide/typography.mdx': () =>
    import(/* webpackPrefetch: true, webpackChunkName: "js-styleguide-typography" */ 'js/styleguide/typography.mdx'),
}

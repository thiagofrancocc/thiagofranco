# Cases - Language Files

This document contains the "schema" of cases language.
Follow the examples.

## Image

The image needs:

- url (required)
- size (required) (large, medium, small)
- caption (optional)

```json
{
  "type": "image",
  "url": "img-example-image-large.jpg",
  "size": "medium",
  "caption": "caption"
}
```

## Block

The block needs:

- title (optional)
- leftTitle (optional)
- leftSubtitle (optional)
- items (required)

```json
{
  "type": "block",
  "title": "Title",
  "leftTitle": "Left Title",
  "leftSubtitle": "Left Subtitle",
  "items": []
}
```

The block contain the following items types:

- subtitle
- paragraph
- image
- title-list
- link
- highlight

### Block/Subtitle

The subtitle needs:

- value (required)

```json
{
  "type": "subtitle",
  "value": "Text Subtitle"
}
```

### Block/Paragraph

The paragraph needs:

- value (required)

```json
{
  "type": "paragraph",
  "value": "Paragraph Text"
}
```

### Block/Image

The image needs:

- url (required)
- size (required) (small)
- caption (optional)

```json
{
  "type": "image",
  "url": "img-example-image-large.jpg",
  "size": "small",
  "caption": "caption"
}
```

### Block/Title List

The title list needs:

- title (required)
- items (required)

```json
{
  "type": "title-list",
  "title": "Title List",
  "items": ["Item 1", "Item 2"]
}
```

### Block/Link

The link needs:

- text (required)
- url (required)
- target (optional)

```json
{
  "type": "link",
  "text": "Text",
  "url": "http://thiagofranco.me",
  "target": "_blank"
}
```

### Block/Highlight

The highlight needs:

- text (required)

```json
{
  "type": "highlight",
  "text": "Text highlight"
}
```

## Highlight Text

The highlight text needs:

- text (required)

```json
{
  "type": "highlight-text",
  "text": "Title"
}
```

## Block Columns

The block needs:

- columns (required)

```json
{
  "type": "block-columns",
  "columns": []
}
```

The block columns contain the following columns types:

- common
- awards

### Block Columns/Common

The common needs:

- title (required)
- description (required)
- isResults (required if is results column)
- items (required)

```json
{
  "type": "common",
  "title": "Title Column",
  "isResults": true,
  "description": "Description Column",
  "items": [
    {
      "title": "Title Item",
      "description": "Description Item"
    }
  ]
}
```

### Block Columns/Awards

The awards needs:

- title (required)
- items (required)
- showListLink (optional)

```json
{
  "type": "awards",
  "title": "Title Column",
  "items": ["Item", "Item 2"],
  "showListLink": true
}
```

import { css } from 'docz-plugin-css';
const { resolve } = require('path');

export default {
  src: './src',
  plugins: [
    css({
      preprocessor: 'sass',
      loaderOpts: {
        includePaths: [
          resolve(__dirname, './src/styles'),
          resolve(__dirname, './src/js'),
          resolve(__dirname, './assets'),
          resolve(__dirname, './node_modules')
        ],
        importLoaders: 1
      }
    })
  ],
  modifyBundlerConfig: config => {
    config.resolve.alias = {
      styles: resolve(__dirname, './src/styles'),
      js: resolve(__dirname, './src/js'),
      assets: resolve(__dirname, './assets')
    };

    return config;
  }
};

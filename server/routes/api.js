const express = require('express');
const cors = require('cors');

const router = express.Router();

router.get('/home', cors(), (req, res) => {
  res.json({ data: 'home' });
});

module.exports = router;

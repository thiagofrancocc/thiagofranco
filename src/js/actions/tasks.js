export const insert = (title = null) => {
  return (dispatch, getState) => {
    return dispatch({
      type: 'TASKS_INSERT',
      payload: { title: title, id: new Date().getTime(), complete: false, dueDate: null }
    });
  };
};

export const edit = (data = null) => {
  return (dispatch, getState) => {
    return dispatch({
      type: 'TASKS_EDIT',
      payload: data
    });
  };
};

export const remove = (data = null) => {
  return (dispatch, getState) => {
    return dispatch({
      type: 'TASKS_DELETE',
      payload: data
    });
  };
};

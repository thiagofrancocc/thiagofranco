import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

const withRoutes = WrapperComponent =>
  class HocComponent extends PureComponent {
    constructor(props) {
      super(props);
    }

    static propTypes = {
      history: PropTypes.object,
      location: PropTypes.object,
      match: PropTypes.object
    };

    getCurrentCase() {
      const { cases } = common;
      const {
        match: {
          params: { name }
        }
      } = this.props;

      return cases.find(item => item.pathname == name);
    }

    getNextCase() {
      const { cases } = common;
      const {
        match: {
          params: { name },
          url
        }
      } = this.props;

      let currentCaseIndex = cases.findIndex(item => item.pathname == name);

      return cases[url == '/' ? 0 : currentCaseIndex + 1];
    }

    isValid(route = null) {
      const { cases } = common;
      const {
        match: {
          params: { name }
        },
        location: { pathname }
      } = this.props;

      let findCase = !!cases.find(item => item.pathname == (route || name));

      return pathname == '/' ? true : findCase;
    }

    render() {
      const newProps = {
        isValid: this.isValid.bind(this),
        getNextCase: this.getNextCase.bind(this),
        getCurrentCase: this.getCurrentCase.bind(this)
      };

      return <WrapperComponent {...this.props} {...newProps} />;
    }
  };

export default withRoutes;

import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

const withLanguage = WrapperComponent =>
  class HocComponent extends PureComponent {
    constructor(props) {
      super(props);
    }

    static propTypes = {
      history: PropTypes.object,
      location: PropTypes.object,
      match: PropTypes.object,
      language: PropTypes.string
    };

    getLanguage(page = null) {
      const { language } = this.props;
      return page ? require(`language/${page}/${language}.json`) : null;
    }

    render() {
      const newProps = {
        getLanguage: this.getLanguage.bind(this)
      };

      return <WrapperComponent {...this.props} {...newProps} />;
    }
  };

export default withLanguage;

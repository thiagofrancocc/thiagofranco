import withLanguage from './Language';
import withRoutes from './Routes';

export { withLanguage, withRoutes };

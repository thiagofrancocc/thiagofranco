// -----------------------------------------------------
// Redux Imports
// -----------------------------------------------------

import { combineReducers } from 'redux';
import { createResponsiveStateReducer } from 'redux-responsive';
import { routerReducer } from 'react-router-redux';

// -----------------------------------------------------
// Load Reducers
// -----------------------------------------------------

import appReducer from 'js/reducers/app';

// -----------------------------------------------------
// Share reducers to application
// -----------------------------------------------------

const reducers = combineReducers({
  router: routerReducer,
  browser: createResponsiveStateReducer({
    mobile: 767,
    tablet: 1023,
    laptop: 1339,
    desktop: 2559
  }),
  app: appReducer
});

export default reducers;

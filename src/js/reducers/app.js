const initialState = {
  language: 'pt-br'
};

const appReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'APP_SET_LANGUAGE':
      return {
        ...state,
        language: action.payload
      };
    default:
      return state;
  }
};

export default appReducer;

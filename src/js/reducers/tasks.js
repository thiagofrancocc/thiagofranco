const initialState = {
  items: []
};

const generalReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'TASKS_INSERT':
      return {
        ...state,
        items: [action.payload, ...state.items]
      };
    case 'TASKS_EDIT':
      return {
        ...state,
        items: state.items.map(task => (task.id === action.payload.id ? action.payload : task))
      };

    case 'TASKS_DELETE':
      return {
        ...state,
        items: state.items.filter(task => task.id != action.payload.id)
      };
    default:
      return state;
  }
};

export default generalReducer;

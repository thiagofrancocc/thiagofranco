import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, Redirect } from 'react-router-dom';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { withRoutes } from 'js/hocs';

import AsyncRoute from 'js/components/async-route';
import Header from 'js/components/Header';
import Footer from 'js/components/Footer';

class LayoutComponent extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    location: PropTypes.object,
    isValid: PropTypes.func
  };

  render() {
    const {
      isValid: routeIsValid,
      location: { pathname }
    } = this.props;
    const { routes } = common;
    const routesList = Object.keys(routes);

    return (
      <Fragment>
        {routesList && (
          <Switch>
            {routesList.map((item, i) => {
              const { exact, component } = routes[item];

              return (
                <Route
                  key={i}
                  path={item}
                  exact={exact}
                  render={props => {
                    const {
                      match: {
                        params: { name }
                      }
                    } = props;
                    return routeIsValid(name) ? (
                      <Fragment key={pathname}>
                        <Header />
                        <AsyncRoute {...props} component={component} />
                        <Footer />
                      </Fragment>
                    ) : (
                      <Redirect to="/" />
                    );
                  }}
                />
              );
            })}
          </Switch>
        )}
      </Fragment>
    );
  }
}

export default compose(
  withRouter,
  withRoutes
)(LayoutComponent);

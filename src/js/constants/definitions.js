export const HEADER_MOBILE_HEIGHT = '6rem';
export const HEADER_DESKTOP_HEIGHT = '17rem';
export const REGISTER_FORM_STORAGE_KEY = 'ag:register-form';

export const VALIDATION_PATTERNS = {
  string: '[a-zA-Z áàâäãåçéèêëíìîïñóòôöõúùûüýÿæœÁÀÂÄÃÅÇÉÈÊËÍÌÎÏÑÓÒÔÖÕÚÙÛÜÝŸÆŒ]{2,}]',
  email: '[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})',
  cep: '[0-9]{5}-[0-9]{3}',
  cpf: '[0-9]{3}.?[0-9]{3}.?[0-9]{3}-?[0-9]{2}',
  rg: '[0-9]{2}.?[0-9]{3}.?[0-9]{3}-?[0-9]{1}',
  number: '[+-]?(?=.)(?:d+,)*d*(?:.d+)?$',
  phone: `([0-9]{2}\) [0-9]{4,6}-[0-9]{3,4}`,
  date: '[0-9]{2}.[0-9]{2}.[0-9]{4}$',
  name: `([A-Za-z]{4,} [A-Za-z]{4,})`
};

export const MASK_OPTIONS = {
  currency: `'alias': 'currency',
  'radixPoint': ',',
  'groupSeparator': '.',
  'allowMinus': 'true',
  'prefix': 'R$ ',
  'digits': '2',
  'maxLength': '13',
  'digitsOptional': 'false',
  'rightAlign': 'false',
  'unmaskAsNumber': 'true'`
};

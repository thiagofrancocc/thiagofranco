export const ROUTES = {
  '/': {
    component: 'Home',
    exact: true
  },
  '/:name': {
    component: 'Case',
    exact: false
  }
};

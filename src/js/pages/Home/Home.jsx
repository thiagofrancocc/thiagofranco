import React, { Fragment } from 'react';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { Helmet } from 'react-helmet';

import { withLanguage, withRoutes } from 'js/hocs';
import CasesNav from 'js/components/CasesNav';
import PhotoFranco from 'assets/images/img-photo-franco.jpg';

const Home = props => {
  return (
    <Fragment>
      <Helmet>
        <title>Thiago Franco</title>
        <meta
          name="description"
          content="Especialista em design de experiência, com foco na criação de plataformas digitais."
        />
      </Helmet>
      <section className="tf-home tf-u-page mdc-layout-grid mdc-layout-grid__inner">
        <CasesNav />
        <div className="tf-home__content mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-6-laptop mdc-layout-grid__cell--span-6-desktop">
          <figure className="tf-home__figure">
            <img src={PhotoFranco} alt="" />
          </figure>
          <div className="mdc-layout-grid__inner">
            <h2 className="tf-fs-large tf-pt-40-mobile tf-pt-64-tablet tf-pt-88-laptop mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-12-tablet mdc-layout-grid__cell--span-9-laptop mdc-layout-grid__cell--span-8-desktop">
              Especialista em design de experiência, com foco na criação de plataformas digitais.
            </h2>
          </div>
          <div className="mdc-layout-grid__inner tf-mt-24-mobile tf-mt-32-tablet tf-mt-40-laptop">
            <div className="mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-8-tablet mdc-layout-grid__cell--span-8-laptop mdc-layout-grid__cell--span-8-desktop">
              <p className="tf-fs-body tf-mt-0-mobile">
                Nos últimos 10+ anos, atuei em empresas proeminentes no mercado global como Work & Co, R/GA, Possible,
                Isobar e Globo.com. Meu papel é usar o potencial da tecnologia criativamente, contribuindo para a
                evolução de marcas e negócios; mas acima de tudo, da sociedade como um todo.
              </p>
              <p className="tf-fs-body tf-mt-24-mobile tf-mt-32-tablet tf-mt-40-laptop">
                Meus principais projetos foram: next um banco que ajuda clientes a controlarem gastos e tomarem melhores
                decisões, TIM beta um plano de telefonia exclusivo para quem tem muito a dizer, que funciona como um
                jogo, e Google Mobile Arcade uma dinâmica educativa para fazer CEOs sentirem na pele a importância da
                presença mobile para seus consumidores. Esses trabalhos receberam nomeações e prêmios em Cannes Lions,
                The One Show, Webby Awards, IxDA Awards e El Ojo de Iberoamérica.
              </p>
              <p className="tf-fs-body tf-mt-24-mobile tf-mt-32-tablet tf-mt-40-laptop">
                Participo da comunidade de UX design na América Latina, com palestras na Interaction Latin ( South )
                America. Fui co-criador do primeiro curso de UX para a Miami Ad School no Brasil. Minha formação: Mestre
                em Arquitetura de Informação e Gestão de Conhecimento pela Kent State University ( EUA ), Bacharel em
                Comunicação Social pela PUC-Rio.
              </p>
            </div>
            <div className="mdc-layout-grid__cell--span-4">
              <ul className="tf-u-list-reset tf-mt-20-mobile tf-mt-0-laptop tf-mb-40-mobile tf-mb-0-laptop">
                <li>
                  <a href="#" className="tf-fs-body tf-u-link tf-u-link--underline">
                    Linkedin
                  </a>
                </li>
                <li>
                  <a href="#" className="tf-fs-body tf-u-link tf-u-link--underline">
                    Twitter
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

const mapStateToProps = state => ({
  language: state.app.language
});

export default compose(
  connect(mapStateToProps),
  withRouter,
  withLanguage,
  withRoutes
)(Home);

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';
import { Helmet } from 'react-helmet';
import { withLanguage } from 'js/hocs';

import HeaderProject from 'js/components/HeaderProject';
import BlockContent from 'js/components/BlockContent';
import Image from 'js/components/Image';
import Subtitle from 'js/components/Subtitle';
import Paragraph from 'js/components/Paragraph';
import TitleList from 'js/components/TitleList';
import Link from 'js/components/Link';
import BlockHighlightText from 'js/components/BlockHighlightText';
import HighlightText from 'js/components/HighlightText';
import BlockColumns from 'js/components/BlockColumns';

class Case extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    language: PropTypes.string,
    match: PropTypes.object,
    getLanguage: PropTypes.func,
    isMobile: PropTypes.bool
  };

  // -----------------------------------------------------------

  renderImage({ url, caption, size, videoUrl }, index) {
    const {
      match: {
        params: { name }
      }
    } = this.props;
    const imageUrl = require(`assets/images/${name}/${url}`);

    return <Image key={index} src={imageUrl} alt="image" size={size} title={caption} videoUrl={videoUrl} />;
  }

  // -----------------------------------------------------------

  renderBlock({ title, leftTitle, leftSubtitle, items }, index) {
    return (
      <BlockContent title={title} key={index} leftTitle={leftTitle} leftSubtitle={leftSubtitle}>
        {items.map((blockItem, i) => {
          switch (blockItem.type) {
            case 'subtitle':
              return <Subtitle key={i}>{blockItem.value}</Subtitle>;
              break;

            case 'paragraph':
              return <Paragraph key={i}>{blockItem.value}</Paragraph>;
              break;

            case 'image':
              return this.renderImage(blockItem, i);
              break;

            case 'title-list':
              return <TitleList key={i} title={blockItem.title} items={blockItem.items} />;
              break;

            case 'link':
              return (
                <Link key={i} url={blockItem.url} target={blockItem.target || ''}>
                  {blockItem.text}
                </Link>
              );
              break;

            case 'highlight':
              return <BlockHighlightText key={i}>{blockItem.text}</BlockHighlightText>;
              break;
          }
        })}
      </BlockContent>
    );
  }

  // -----------------------------------------------------------

  render() {
    const {
      match: {
        params: { name }
      },
      getLanguage
    } = this.props;
    const CaseLanguage = getLanguage(name);
    const { title, description, content } = CaseLanguage;

    return (
      <section className="tf-project tf-u-page mdc-layout-grid">
        <Fragment>
          <Helmet>
            <title>Thiago Franco - {title}</title>
            <meta name="description" content={description} />
          </Helmet>
          <HeaderProject title={title} description={description} />
          {content.map((item, index) => {
            switch (item.type) {
              case 'image':
                return this.renderImage(item, index);
                break;

              case 'block':
                return this.renderBlock(item, index);
                break;

              case 'highlight-text':
                return <HighlightText key={index}>{item.text}</HighlightText>;
                break;

              case 'block-columns':
                return <BlockColumns key={index} columns={item.columns} />;
                break;
            }
          })}
        </Fragment>
      </section>
    );
  }
}

const mapStateToProps = state => ({
  isMobile: state.browser.is.mobile,
  language: state.app.language,
  router: state.router.location
});

export default compose(
  connect(mapStateToProps),
  withRouter,
  withLanguage
)(Case);

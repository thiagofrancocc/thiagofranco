import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withLanguage, withRoutes } from 'js/hocs';

import Image from 'js/components/Image';

const Footer = ({ isMobile, getNextCase, getLanguage, history: { push } }) => {
  const nextCase = getNextCase();
  const nextCaseLanguage = nextCase ? getLanguage(nextCase.pathname) : null;
  const imageUrl = nextCase ? require(`assets/images/${nextCase.pathname}/${nextCaseLanguage.content[0].url}`) : null;
  const classesFooter = classNames('tf-mt-big-mobile tf-footer', {
    ['tf-footer--nav']: !nextCase
  });

  return (
    <footer
      className={classesFooter}
      onClick={evt => (nextCase ? push(`/${nextCase.pathname}`) : evt.preventDefault())}
    >
      {nextCase ? (
        <div className="mdc-layout-grid mdc-layout-grid__inner">
          <h3 className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-3-laptop mdc-layout-grid__cell--span-3-desktop tf-fs-normal tf-color-dark-gray">
            Ver Case
          </h3>
          <div className="tf-pt-40-mobile tf-pt-0-tablet mdc-layout-grid__cell mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-9-laptop mdc-layout-grid__cell--span-9-desktop">
            <h2 className="tf-fs-normal">{nextCaseLanguage.title}</h2>
            <div className="mdc-layout-grid__inner">
              <p className="mdc-layout-grid__cell mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-12-tablet mdc-layout-grid__cell--span-7-laptop mdc-layout-grid__cell--span-7-desktop tf-fs-big tf-pt-20-mobile tf-pt-30-tablet">
                {nextCaseLanguage.description}
              </p>
            </div>
          </div>
          {!isMobile && (
            <div className="tf-pt-80-mobile tf-pt-0-tablet mdc-layout-grid__cell mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-12-tablet mdc-layout-grid__cell--span-12-laptop mdc-layout-grid__cell--span-12-desktop">
              {imageUrl && <Image src={imageUrl} alt="image" size="large" className="tf-footer__image" />}
            </div>
          )}
        </div>
      ) : (
        <nav className="tf-footer__nav">
          <div className="mdc-layout-grid mdc-layout-grid__inner">
            <a
              href="#"
              className="tf-footer__links tf-fs-body mdc-layout-grid__cell--span-2-mobile mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-9-laptop mdc-layout-grid__cell--span-9-desktop tf-u-link tf-u-link--underline"
            >
              English
            </a>
            <div className="tf-footer__links mdc-layout-grid__cell--span-2-mobile mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-3-laptop mdc-layout-grid__cell--span-3-desktop">
              <a href="#" className="tf-fs-body tf-u-link tf-u-link--underline tf-mr-20-mobile">
                Linkedin
              </a>
              <a href="#" className="tf-fs-body tf-u-link tf-u-link--underline">
                Twitter
              </a>
            </div>
          </div>
        </nav>
      )}
    </footer>
  );
};

Footer.propTypes = {
  match: PropTypes.object,
  history: PropTypes.object,
  language: PropTypes.string,
  isMobile: PropTypes.bool,
  getLanguage: PropTypes.func,
  getNextCase: PropTypes.func
};

const mapStateToProps = state => ({
  language: state.app.language,
  isMobile: state.browser.is.mobile
});

export default compose(
  connect(mapStateToProps),
  withRouter,
  withLanguage,
  withRoutes
)(Footer);

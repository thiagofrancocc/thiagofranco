import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';

const BlockLink = ({ url, children, className, ...rest }) => {
  const getStyles = classNames(
    'tf-u-link',
    'tf-u-link--underline',
    'tf-fs-normal',
    'tf-u-d-block',
    'tf-mt-30-mobile',
    'tf-mt-64-laptop',
    className
  );

  return (
    <Link to={url} className={getStyles} {...rest}>
      {children}
    </Link>
  );
};

BlockLink.propTypes = {
  className: PropTypes.string,
  url: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

export default BlockLink;

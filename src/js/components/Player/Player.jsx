import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import IconClose from 'js/components/Icons/IconClose';

class Player extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    className: PropTypes.string,
    show: PropTypes.bool.isRequired,
    url: PropTypes.string.isRequired,
    onClose: PropTypes.func
  };

  static defaultProps = {
    show: false,
    url: ''
  };

  onClose = () => {
    const { onClose } = this.props;

    if (onClose) onClose();
  };

  render() {
    const { className, show } = this.props;
    const getStyles = classNames('tf-player', className);
    const modalEl = document.querySelector('.tf-modal');

    if (!show) return null;

    return ReactDOM.createPortal(
      <div className={getStyles} onClick={this.onClose}>
        <div className="tf-player__video" onClick={e => e.stopPropagation()}>
          <iframe
            src="https://www.youtube.com/embed/INbxNcAI7J0?rel=0&amp;showinfo=0"
            frameBorder="0"
            allow="autoplay; encrypted-media"
            allowFullScreen
          />
        </div>
        <button className="tf-player__close-btn" role="button" onClick={this.onClose}>
          <IconClose className="tf-player__close-icon" />
        </button>
      </div>,
      modalEl
    );
  }
}

export default Player;

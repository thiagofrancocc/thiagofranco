import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const BlockHighlightText = ({ children, className }) => {
  const getStyles = classNames('tf-fs-normal', 'tf-mb-30-mobile', 'tf-mb-40-laptop', className);

  return <h3 className={getStyles}>{children}</h3>;
};

BlockHighlightText.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

export default BlockHighlightText;

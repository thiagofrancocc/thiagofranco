import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Subtitle = ({ children, className }) => {
  const getStyles = classNames('tf-fs-small', 'tf-mb-10-mobile', 'tf-mb-24-laptop', className);

  return (
    <h4 className={getStyles}>
      <strong>{children}</strong>
    </h4>
  );
};

Subtitle.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

export default Subtitle;

import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRoutes } from 'js/hocs';

class Header extends Component {
  constructor(props) {
    super(props);

    this.prevScrollpos = window.pageYOffset;
  }

  static propTypes = {
    language: PropTypes.string,
    location: PropTypes.object,
    match: PropTypes.object,
    isMobile: PropTypes.bool,
    getNextCase: PropTypes.func,
    getCurrentCase: PropTypes.func
  };

  componentDidMount() {
    this.registerScrollEvents();
  }

  registerScrollEvents() {
    window.addEventListener('scroll', this.showHide);
  }

  showHide = () => {
    const {
      location: { pathname },
      isMobile
    } = this.props;

    if ((pathname == '/' && isMobile) || pathname != '/') {
      let currentScrollPos = window.pageYOffset;

      if (this.header) {
        this.prevScrollpos > currentScrollPos
          ? this.header.classList.remove('tf-header--hide')
          : this.header.classList.add('tf-header--hide');

        this.prevScrollpos = currentScrollPos;
      }
    }
  };

  getClasses() {
    const {
      match: { params },
      getNextCase
    } = this.props;
    const nextCase = getNextCase();

    return {
      header: classNames('tf-header mdc-layout-grid', {
        ['tf-header--home']: !params.name
      }),
      title: classNames('tf-header__logo mdc-layout-grid__cell--span-3-mobile mdc-layout-grid__cell--span-2-tablet', {
        ['mdc-layout-grid__cell--span-5-laptop']: !params.name,
        ['mdc-layout-grid__cell--span-3-laptop']: params.name,
        ['mdc-layout-grid__cell--span-5-desktop']: !params.name,
        ['mdc-layout-grid__cell--span-3-desktop']: params.name
      }),
      caseName: classNames('tf-fs-normal mdc-layout-grid__cell--span-4-mobile', {
        ['mdc-layout-grid__cell--span-4-tablet']: !nextCase,
        ['mdc-layout-grid__cell--span-3-tablet']: nextCase,
        ['mdc-layout-grid__cell--span-7-laptop']: !nextCase,
        ['mdc-layout-grid__cell--span-5-laptop']: nextCase,
        ['mdc-layout-grid__cell--span-7-desktop']: !nextCase,
        ['mdc-layout-grid__cell--span-5-desktop']: nextCase
      }),
      nextCaseLink: classNames(
        'tf-header__next-case-link tf-fs-body tf-u-link tf-fs-normal mdc-layout-grid__cell--span-1-mobile mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-4-laptop mdc-layout-grid__cell--span-4-desktop'
      )
    };
  }

  render() {
    const {
      location: { pathname },
      isMobile,
      getNextCase,
      getCurrentCase
    } = this.props;
    const classes = this.getClasses();
    const nextCaseData = getNextCase();
    const currentCaseData = getCurrentCase();

    return (
      <header
        className={classes.header}
        ref={e => {
          this.header = e;
        }}
      >
        <nav className="tf-u-w-100 tf-header__inner mdc-layout-grid__inner">
          <h1 className={classes.title}>
            <Link to="/" className="tf-color-black">
              Thiago Franco
            </Link>
          </h1>
          {pathname == '/' ? (
            <a
              href="#"
              className="tf-header__english-link tf-u-link tf-u-link--underline tf-fs-body mdc-layout-grid__cell--span-1-mobile mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-7-laptop"
            >
              English
            </a>
          ) : (
            <Fragment>
              {!isMobile && <h2 className={classes.caseName}>{currentCaseData.name}</h2>}
              {nextCaseData && (
                <Link className={classes.nextCaseLink} to={`/${nextCaseData ? nextCaseData.pathname : ''}`}>
                  Próximo
                </Link>
              )}
            </Fragment>
          )}
        </nav>
      </header>
    );
  }
}

const mapStateToProps = state => ({
  isMobile: state.browser.is.mobile,
  language: state.app.language,
  router: state.router.location
});

export default compose(
  connect(mapStateToProps),
  withRouter,
  withRoutes
)(Header);

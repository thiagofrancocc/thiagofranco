import React from 'react';
import PropTypes from 'prop-types';
import Loadable from 'react-loadable';

const Loading = () => <div />;

const AsyncRoute = props => {
  window.scrollTo(0, 0);
  const { component } = props;

  const AsyncComponent =
    process.env.NODE_ENV == 'development'
      ? require(`js/pages/${component}`).default
      : Loadable({
          loader: () => import(`js/pages/${component}`),
          loading: Loading
        });

  return <AsyncComponent />;
};

AsyncRoute.propTypes = {
  component: PropTypes.string.isRequired
};

export default AsyncRoute;

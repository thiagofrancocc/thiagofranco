import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { compose } from 'redux';
import { withRouter } from 'react-router';

class CasesNav extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    isNotMobile: PropTypes.bool,
    history: PropTypes.object
  };

  state = {
    registeredScrollEvents: false
  };

  componentDidMount() {
    this.registerHistoryListener();
    this.registerScrollEvents();
  }

  componentDidUpdate() {
    this.registerScrollEvents();
  }

  registerHistoryListener() {
    const { history } = this.props;

    history.listen(location => {
      if (location.pathname != '/') this.removeScrollEvents();
    });
  }

  registerScrollEvents() {
    const { isNotMobile } = this.props;
    const { registeredScrollEvents } = this.state;

    if (!registeredScrollEvents && isNotMobile) {
      window.addEventListener('scroll', this.handleScroll);
      this.setState({ registeredScrollEvents: true });
    }
  }

  removeScrollEvents() {
    window.removeEventListener('scroll', this.handleScroll);
  }

  handleScroll = event => {
    const footerEl = document.querySelector('.tf-footer');
    const casesNavEl = document.querySelector('.tf-cases-nav');

    let wH = window.innerHeight;
    let offset = 0;
    let yPosition = footerEl.getBoundingClientRect().top - wH;

    if (casesNavEl) {
      yPosition <= offset
        ? casesNavEl.classList.add('tf-cases-nav--absolute')
        : casesNavEl.classList.remove('tf-cases-nav--absolute');
    }
  };

  render() {
    const { cases } = common;

    return (
      <aside className="tf-cases-nav mdc-layout-grid__cell mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-3-tablet mdc-layout-grid__cell--span-6-laptop mdc-layout-grid__cell--span-6-desktop">
        <dl className="tf-cases-nav__list">
          <dt className="tf-cases-nav__title tf-f-regular tf-fs-normal">Cases</dt>
          {cases && (
            <Fragment>
              {cases.map(({ name, pathname }, i) => {
                return (
                  <dd key={i} className="tf-cases-nav__item">
                    <Link to={`/${pathname}`} className="tf-u-link">
                      {name}
                    </Link>
                  </dd>
                );
              })}
            </Fragment>
          )}
        </dl>
      </aside>
    );
  }
}

const mapStateToProps = state => ({
  isNotMobile: state.browser.greaterThan.mobile
});

export default compose(
  connect(mapStateToProps),
  withRouter
)(CasesNav);

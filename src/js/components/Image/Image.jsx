import React, { Fragment, Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import Player from 'js/components/Player';

class Image extends Component {
  constructor(props) {
    super(props);
  }

  static propTypes = {
    className: PropTypes.string,
    title: PropTypes.string,
    type: PropTypes.string,
    videoUrl: PropTypes.string,
    size: PropTypes.oneOf(['large', 'medium', 'small']),
    src: PropTypes.string.isRequired
  };

  static defaultProps = {
    className: '',
    title: null,
    type: null,
    videoUrl: '',
    size: 'medium'
  };

  state = {
    playerIsOpen: false
  };

  getClasses = params => classNames(params);

  onClickImage = evt => {
    evt.preventDefault();
    const { videoUrl } = this.props;

    if (videoUrl) {
      this.setState({
        playerIsOpen: true
      });
    }
  };

  onClosePlayer = () => {
    this.setState({
      playerIsOpen: false
    });
  };

  render() {
    const { className, size, title, type, videoUrl, ...rest } = this.props;
    const { playerIsOpen } = this.state;
    const containerClasses = this.getClasses([
      'tf-image',
      'tf-mt-60-mobile',
      'tf-mb-60-mobile',
      'tf-mt-96-laptop',
      'tf-mb-96-laptop',
      'mdc-layout-grid__inner',
      className,
      {
        [`tf-image--${type}`]: type,
        [`tf-image--${size}`]: size,
        [`tf-image--player`]: videoUrl
      }
    ]);

    const imgClasses = this.getClasses([
      'tf-image__photo',
      'mdc-layout-grid__cell--span-4-mobile',
      'mdc-layout-grid__cell--span-6-tablet',
      {
        [`mdc-layout-grid__cell--span-12-laptop`]: size == 'large' || size == 'small' || (size == 'medium' && !title),
        [`mdc-layout-grid__cell--span-12-desktop`]: size == 'large' || size == 'small' || (size == 'medium' && !title),
        [`mdc-layout-grid__cell--span-9-laptop`]: size == 'medium' && title,
        [`mdc-layout-grid__cell--span-9-desktop`]: size == 'medium' && title,
        [`tf-image__photo--right`]: size == 'medium' && title
      }
    ]);

    const titleClasses = this.getClasses([
      'tf-image__title',
      'tf-fs-caption',
      'mdc-layout-grid__cell--span-4-mobile',
      {
        ['tf-image__title--bottom']: size == 'medium',
        ['mdc-layout-grid__cell--span-2-tablet']: size != 'small',
        ['mdc-layout-grid__cell--span-4-tablet']: size == 'small',
        ['mdc-layout-grid__cell--span-3-laptop']: size != 'small',
        ['mdc-layout-grid__cell--span-7-laptop']: size == 'small',
        ['mdc-layout-grid__cell--span-3-desktop']: size != 'small',
        ['mdc-layout-grid__cell--span-7-desktop']: size == 'small'
      }
    ]);

    return (
      <Fragment>
        <div className={containerClasses} onClick={this.onClickImage}>
          <figure className={imgClasses}>
            <img {...rest} />
          </figure>
          {title && <span className={titleClasses}>{title}</span>}
        </div>
        {playerIsOpen && <Player show={playerIsOpen} onClose={this.onClosePlayer} />}
      </Fragment>
    );
  }
}

export default Image;

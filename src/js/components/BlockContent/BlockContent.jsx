import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const BlockContent = ({ className, title, leftTitle, leftSubtitle, children }) => {
  const getClasses = classNames(
    'tf-block-content',
    'mdc-layout-grid__inner',
    'tf-mt-60-mobile',
    'tf-mb-60-mobile',
    'tf-mt-96-laptop',
    'tf-mb-96-laptop',
    className,
    {
      ['tf-pt-56-mobile']: leftTitle,
      ['tf-pt-64-tablet']: leftTitle,
      ['tf-pt-88-laptop']: leftTitle,
      ['tf-border-top-thin']: leftTitle,
      ['tf-border-top-color-gray']: leftTitle
    }
  );

  const getGridLeftClasses = classNames(
    'mdc-layout-grid__cell',
    'mdc-layout-grid__cell--span-4-mobile',
    'mdc-layout-grid__cell--span-2-tablet',
    'mdc-layout-grid__cell--span-6-laptop',
    'mdc-layout-grid__cell--span-6-desktop',
    className,
    {
      ['tf-d-none-mobile']: leftTitle == '',
      ['tf-d-block-tablet']: leftTitle == ''
    }
  );

  return (
    <div className={getClasses}>
      <div className={getGridLeftClasses}>
        {leftTitle && (
          <Fragment>
            <p className="tf-fs-body tf-mg-0-mobile">
              <b>{leftTitle}</b>
            </p>
            {leftSubtitle && <p className="tf-fs-body tf-mt-0-mobile">{leftSubtitle}</p>}
          </Fragment>
        )}
      </div>
      <div className="tf-block-content__container mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-6-laptop mdc-layout-grid__cell--span-6-desktop">
        {title && <h3 className="tf-fs-large tf-mb-30-mobile tf-mb-44-laptop">{title}</h3>}
        {children}
      </div>
    </div>
  );
};

BlockContent.propTypes = {
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired,
  className: PropTypes.string,
  title: PropTypes.string,
  leftTitle: PropTypes.string,
  leftSubtitle: PropTypes.string
};

BlockContent.defaultProps = {
  className: '',
  title: '',
  leftTitle: '',
  leftSubtitle: ''
};

export default BlockContent;

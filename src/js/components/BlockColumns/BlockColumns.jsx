import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const CommonColumn = ({ title, description, isResults, items }) => {
  const dtClass = classNames('tf-block-columns__list-title tf-fs-body', {
    ['tf-block-columns__list-title--results']: isResults
  });

  const ddClass = classNames('tf-block-columns__list-desc tf-fs-body tf-mg-0-mobile tf-mb-30-mobile tf-mb-43-laptop', {
    ['tf-block-columns__list-desc--results']: isResults
  });

  return (
    <Fragment>
      <h2 className="tf-fs-normal tf-mb-30-mobile tf-mb-47-laptop">{title}</h2>
      <p className="tf-fs-body tf-mb-30-mobile tf-mb-43-laptop">{description}</p>
      <dl className="tf-block-columns__list tf-mg-0-mobile">
        {items.map(({ title, description }, i) => {
          return (
            <Fragment key={i}>
              <dt className={dtClass}>
                <strong>{title}</strong>
              </dt>
              <dd className={ddClass}>{description}</dd>
            </Fragment>
          );
        })}
      </dl>
    </Fragment>
  );
};

CommonColumn.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  isResults: PropTypes.bool.isRequired
};

CommonColumn.defaultProps = {
  isResults: false
};

// -----------------------------------------------------------

const AwardsColumn = ({ title, items, showListLink }) => {
  return (
    <Fragment>
      <h2 className="tf-fs-normal tf-mb-30-mobile tf-mb-47-laptop">{title}</h2>
      <ul className="tf-u-list-reset">
        {items.map((item, i) => {
          return (
            <li key={i} className="tf-fs-body">
              {item}
            </li>
          );
        })}
      </ul>
      {showListLink && (
        <p className="tf-mt-30-mobile tf-mt-40-laptop tf-fs-body">
          Acesse a lista completa de prêmios no meu perfil do{' '}
          <a href="#" target="_blank" className="tf-u-link tf-u-link--underline">
            Linkedin
          </a>
          .
        </p>
      )}
    </Fragment>
  );
};

AwardsColumn.propTypes = {
  title: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired,
  showListLink: PropTypes.bool
};

AwardsColumn.defaultProps = {
  showListLink: true
};

// -----------------------------------------------------------

const BlockColumns = props => {
  const { columns, className } = props;
  const getStyles = classNames('mdc-layout-grid__inner', 'tf-mt-0-mobile', 'tf-mt-0-tablet', className);

  return (
    <div className={getStyles}>
      {columns.map(({ type, ...rest }, i) => {
        const columnClass = classNames(
          'tf-block-columns tf-mt-0-tablet mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-4-laptop mdc-layout-grid__cell--span-4-desktop',
          {
            ['tf-mt-40-mobile']: i > 0
          }
        );

        return (
          <div key={i} className={columnClass}>
            {type == 'common' && <CommonColumn {...rest} {...props} />}
            {type == 'awards' && <AwardsColumn {...rest} {...props} />}
          </div>
        );
      })}
    </div>
  );
};

BlockColumns.propTypes = {
  className: PropTypes.string,
  columns: PropTypes.array.isRequired
};

export default BlockColumns;

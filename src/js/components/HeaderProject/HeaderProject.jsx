import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const HeaderProject = ({ className, title, description }) => {
  const getClasses = classNames('tf-project-header', 'mdc-layout-grid__inner', className);

  return (
    <header className={getClasses}>
      <div className="tf-d-none-mobile tf-d-block-tablet mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-2-tablet mdc-layout-grid__cell--span-3-laptop mdc-layout-grid__cell--span-3-desktop" />
      <div className="mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-4-tablet mdc-layout-grid__cell--span-9-laptop mdc-layout-grid__cell--span-9-desktop">
        <h2 className="tf-project-header__title tf-fs-normal">{title}</h2>
        <p className="tf-fs-big mdc-layout-grid__inner tf-mt-20-mobile tf-mt-10-tablet tf-mb-0-mobile tf-mb-120-tablet tf-mb-96-laptop">
          <span className="tf-pt-regular-mobile tf-pt-medium-tablet mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-12-tablet mdc-layout-grid__cell--span-6-laptop mdc-layout-grid__cell--span-6-desktop">
            {description}
          </span>
        </p>
      </div>
    </header>
  );
};

HeaderProject.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired
};

HeaderProject.defaultProps = {
  title: '',
  description: ''
};

export default HeaderProject;

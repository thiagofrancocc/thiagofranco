import React from 'react';

const IconArrow = props => (
  <svg viewBox="0 0 16 20" {...props}>
    <path d="M0 20l15.715-10L0 0z" fill="#1A1A1A" fillRule="evenodd" />
  </svg>
);

export default IconArrow;

import React from 'react';

const IconClose = props => (
  <svg viewBox="0 0 14 14" {...props}>
    <path
      d="M7 5.59L1.41 0 0 1.41l5.59 5.589L0 12.589 1.41 14 7 8.41 12.59 14 14 12.589l-5.59-5.59L14 1.41 12.59 0z"
      fill="#1A1A1A"
      fillRule="evenodd"
    />
  </svg>
);

export default IconClose;

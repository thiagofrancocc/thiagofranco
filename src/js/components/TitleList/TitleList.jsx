import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const TitleList = ({ title, items, className }) => {
  const getStyles = classNames(
    'tf-title-list',
    'tf-mt-20-mobile',
    'tf-mb-30-mobile',
    'tf-pl-20-mobile',
    'tf-mt-32-laptop',
    'tf-mb-80-laptop',
    className
  );

  return (
    <Fragment>
      <h4 className="tf-fs-small tf-mt-30-mobile tf-mt-80-laptop">{title}</h4>
      <ul className={getStyles}>
        {items.map((item, i) => (
          <li key={i} className="tf-f-medium tf-fs-small">
            {item}
          </li>
        ))}
      </ul>
    </Fragment>
  );
};

TitleList.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string.isRequired,
  items: PropTypes.array.isRequired
};

export default TitleList;

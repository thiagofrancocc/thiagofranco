import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const HighlightText = ({ className, children }) => {
  const getClasses = classNames(
    'tf-highlight-text',
    'tf-mt-60-mobile',
    'tf-mb-60-mobile',
    'tf-mt-96-laptop',
    'tf-mb-96-laptop',
    'mdc-layout-grid__inner',
    className
  );

  return (
    <div className={getClasses}>
      <div className="tf-d-none-mobile tf-d-block-tablet mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-1-tablet mdc-layout-grid__cell--span-3-laptop mdc-layout-grid__cell--span-3-desktop" />
      <h2 className="tf-fs-big mdc-layout-grid__cell--span-4-mobile mdc-layout-grid__cell--span-5-tablet mdc-layout-grid__cell--span-9-laptop mdc-layout-grid__cell--span-9-desktop">
        {children}
      </h2>
    </div>
  );
};

HighlightText.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

export default HighlightText;

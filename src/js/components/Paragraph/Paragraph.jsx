import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

const Paragraph = ({ children, className }) => {
  const getStyles = classNames(
    'tf-fs-body',
    'tf-mb-30-mobile',
    'tf-mt-0-mobile',
    'tf-mb-40-laptop',
    'tf-mt-0-laptop',
    className
  );

  return <p className={getStyles} dangerouslySetInnerHTML={{ __html: children }} />;
};

Paragraph.propTypes = {
  className: PropTypes.string,
  children: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]).isRequired
};

export default Paragraph;

import store from 'js/stores/configure';

export const getStore = (prop = null) => {
  let newState = { ...store().getState() };

  return prop ? newState[prop] : null;
};

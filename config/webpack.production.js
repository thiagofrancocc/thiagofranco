const { resolve } = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJsWebpackPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

const commonConfig = require('./common.json');

module.exports = {
  entry: ['babel-polyfill', 'whatwg-fetch', resolve(__dirname, '../src/js/app.jsx')],
  devtool: false,
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader' },
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: resolve(__dirname, '../postcss.config.js')
              }
            }
          },
          {
            loader: 'sass-loader?sourceMap',
            options: {
              includePaths: [
                resolve(__dirname, '../src/styles'),
                resolve(__dirname, '../src/js'),
                resolve(__dirname, '../assets'),
                resolve(__dirname, '../node_modules')
              ],
              importLoaders: 1
            }
          }
        ]
      }
    ]
  },
  optimization: {
    minimizer: [
      new UglifyJsWebpackPlugin({
        uglifyOptions: {
          safari10: true
        }
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  },
  plugins: [
    new CleanWebpackPlugin(['dist'], {
      root: resolve(__dirname, '../'),
      allowExternal: true
    }),
    new HtmlWebpackPlugin({
      title: commonConfig.title,
      description: commonConfig.description,
      keywords: commonConfig.keywords,
      og: commonConfig.og,
      namespace: commonConfig.namespace,
      template: '../src/index.html',
      filename: 'index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'styles_[hash].css',
      chunkFilename: '[name]_[id].css'
    })
  ]
};
